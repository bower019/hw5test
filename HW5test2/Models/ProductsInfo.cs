﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Xamarin.Forms;

namespace HW5test2.Models
{
    public class ProductsInfo 
    {
        public partial class ProductsData
        {
            [JsonProperty("productId")]
            public long ProductId { get; set; }

            [JsonProperty("productName")]
            public string ProductName { get; set; }

            [JsonProperty("productCode")]
            public string ProductCode { get; set; }

            [JsonProperty("releaseDate")]
            public string ReleaseDate { get; set; }

            [JsonProperty("description")]
            public string Description { get; set; }

            [JsonProperty("price")]
            public double Price { get; set; }

            [JsonProperty("starRating")]
            public double StarRating { get; set; }

            [JsonProperty("imageUrl")]
            public Uri ImageUrl { get; set; }
        
    }
    }
}

