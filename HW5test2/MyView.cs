﻿using System;

using Xamarin.Forms;

namespace HW5test2
{
    public class MyView : ContentView
    {
        public MyView()
        {
            Content = new Label { Text = "Hello ContentView" };
        }
    }
}

